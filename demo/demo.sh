#! /usr/bin/env bash

#
# Author: Leandro Beretta <lberetta@redhat.com>
#
# Script to create the environments and pipelines configuration.
#

oc login -u system:admin

oc create -f /c/work/openshift/application-templates/secrets/eap7-app-secret.json

oc create -f /c/work/openshift/application-templates/jboss-image-streams.json -n openshift

oc login -u admin -p admin

#
# Jenkins
#

oc new-project jenkins --display-name=JENKINS
oc new-app --template=jenkins-persistent -p MEMORY_LIMIT=1Gi -l app=jenkins

#oc secrets new scmsecret ssh-privatekey=/c/Users/dgauda/.ssh/id_rsa secret/scmsecret
#oc secrets link builder scmsecret

oc secrets -n jenkins new-sshauth sshsecret --ssh-privatekey=/c/Users/dgauda/.ssh/id_rsa
oc secrets -n jenkins add serviceaccount/builder secrets/sshsecret

#oc new-app -n jenkins git@github.com:private-repo-name --name test-pipeline --source-secret=sshsecret

#
# Environments creation
#

oc new-project dev --display-name=DEV
oc new-project test --display-name=TEST
oc new-project prod --display-name=PROD

# Grant edit access to developer in dev project
oc adm policy add-role-to-user edit developer -n dev

# Grant view access to developer in test project
oc adm policy add-role-to-user view developer -n test

# Grant view access to developer in prod project
oc adm policy add-role-to-user view developer -n prod

# Grant view access to developer in jenkins project
oc adm policy add-role-to-user edit developer -n jenkins

# Grant edit access to jenkins service account
oc policy add-role-to-user edit system:serviceaccount:jenkins:jenkins -n dev
oc policy add-role-to-user edit system:serviceaccount:jenkins:jenkins -n test
oc policy add-role-to-user edit system:serviceaccount:jenkins:jenkins -n prod

# Allow prod service account the ability to pull images from dev
oc policy add-role-to-group system:image-puller system:serviceaccounts:test -n dev
oc policy add-role-to-group system:image-puller system:serviceaccounts:prod -n dev

#
# Application deployment
#

#
# Test application
#

oc project dev

# Creates a binary build (the build is not started immediately)
oc new-build --binary=true --name="app" jboss-eap70-openshift:1.5 --allow-missing-imagestream-tags

#oc set label app=app

# Creates the application
oc new-app dev/app:DevCandidate-1.0.0 --name="app" --allow-missing-imagestream-tags=true

# Removes the triggers
oc set triggers dc/app --remove-all

oc expose dc/app --port 8080
oc expose svc/app

#
# Test application
#

oc project test

oc new-app dev/app:TestCandidate-1.0.0 --name="app" --allow-missing-imagestream-tags=true

# Removes the triggers
oc set triggers dc/app --remove-all

oc expose dc/app --port 8080
oc expose svc/app

#
# Production applications
#

# Blue/Green

oc project prod

# Creates the blue and green applications (observe that in prod is not a BuildConfig object created)
oc new-app dev/app:ProdReady-1.0.0 --name="app-green" --allow-missing-imagestream-tags=true
oc new-app dev/app:ProdReady-1.0.0 --name="app-blue" --allow-missing-imagestream-tags=true

# Removes the triggers
oc set triggers dc/app-green --remove-all
oc set triggers dc/app-blue --remove-all

oc expose dc/app-blue --port 8080
oc expose dc/app-green --port 8080

oc expose svc/app-green --name blue-green

#
# Pipelines deployment
#

oc project jenkins

#oc set build-secret --pull bc/ci-pipeline jingalika

# Creates the pipelines
oc new-app https://dgauda@bitbucket.org/dgauda/myprivaterepository#master --context-dir=pipelines/ci --name ci-pipeline --source-secret=sshsecret
oc new-app https://dgauda@bitbucket.org/dgauda/myprivaterepository#master --context-dir=pipelines/cd --name cd-pipeline --source-secret=sshsecret
oc new-app https://dgauda@bitbucket.org/dgauda/myprivaterepository#master --context-dir=pipelines/bg --name bg-pipeline --source-secret=sshsecret
oc new-app https://dgauda@bitbucket.org/dgauda/myprivaterepository#master --context-dir=pipelines/ab --name ab-pipeline --source-secret=sshsecret