package com.redhat.gps.model;

/**
 * Created by lberetta on 3/9/18.
 */
public class Health {

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
