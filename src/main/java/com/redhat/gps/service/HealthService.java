package com.redhat.gps.service;

import com.redhat.gps.model.Health;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by lberetta on 3/9/18.
 */
@Path("health")
public class HealthService {

    private Health health;

    @GET
    @Path("/set")
    @Produces(MediaType.APPLICATION_JSON)
    public Response setHealth(@QueryParam("status") String status) {
        health.setStatus(status);

        return Response.ok(health).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response health() {
        if (health.getStatus().equals("SUCCESS"))
            return Response.ok(health).build();
        else
            return Response.serverError().build();
    }
}
